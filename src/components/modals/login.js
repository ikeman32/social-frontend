import React from "react";
import ReactModal from "react-modal";

import '../../css/modals.css';

class Login extends React.Component {
  constructor() {
    super();
    this.state = {
      showModal: false
    };

    this.handleOpenModal = this.handleOpenModal.bind(this);
    this.handleCloseModal = this.handleCloseModal.bind(this);
  }

  handleOpenModal() {
    this.setState({ showModal: true });
  }

  handleCloseModal() {
    this.setState({ showModal: false });
  }

  render() {
    return (
      <div className='modal'>
        <button id='login-button' onClick={this.handleOpenModal}>Login</button>
        <ReactModal
          isOpen={this.state.showModal}
          contentLabel="Minimal Modal Example"
          className='Login'
        >
          <form id='login-form'>
            <input type='text' name='username' placeholder='Username'/><br></br>
            <input type='password' name='password' placeholder='Password'/><br></br>
            <button onClick={this.handleCloseModal}>Close Modal</button>
          </form>
        </ReactModal>
      </div>
    );
  }
}

export default Login;
