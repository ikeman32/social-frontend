import React from "react";
import { Link, Route } from "react-router-dom";
import logo from "./img/logo.svg";
import "./css/App.css";

import Login from "./components/modals/login";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <h1>My Social</h1>
        <Login />
      </header>

      <Route exact path="/" />
    </div>
  );
}

export default App;
